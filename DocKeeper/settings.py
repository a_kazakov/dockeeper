"""
Django settings for DocKeeper project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
try:
    from DocKeeper.settings_prod import *
except ImportError:
    import os
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))


    # Quick-start development settings - unsuitable for production
    # See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = '51rs9i4sbwgbj6djwd*+&nic2vwlo1xg9k8w7n7ol5bxo3356l'

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = True

    TEMPLATE_DEBUG = True

    ALLOWED_HOSTS = []


    # Application definition

    INSTALLED_APPS = (
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django_extensions',
        'widget_tweaks',
        'Documents',
    )

    MIDDLEWARE_CLASSES = (
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )

    ROOT_URLCONF = 'DocKeeper.urls'

    WSGI_APPLICATION = 'DocKeeper.wsgi.application'


    # Database
    # https://docs.djangoproject.com/en/1.7/ref/settings/#databases

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

    # Internationalization
    # https://docs.djangoproject.com/en/1.7/topics/i18n/

    LANGUAGE_CODE = 'ru-RU'

    TIME_ZONE = 'Europe/Moscow'

    USE_I18N = False

    USE_L10N = False

    USE_TZ = True


    DATETIME_FORMAT = "d.m.Y H:i"
    DATE_FORMAT = "d.m.Y"
    CANONICAL_DATE_FORMAT = "Y-m-d"

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/1.7/howto/static-files/

    _PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    STATICFILES_DIRS = ['static']

    MEDIA_ROOT = os.path.join(_PATH, 'static', 'media')
    MEDIA_URL  = '/static/media/'

    #STATIC_ROOT = os.path.join(_PATH, 'static')
    STATIC_URL = '/static/'

    TMP_DIR = os.path.join(_PATH, 'temp')

    TEMPLATE_DIRS = (
        os.path.join(BASE_DIR,  'templates'),
    )