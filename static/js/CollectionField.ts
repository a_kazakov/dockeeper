/// <reference path="../../thirdparty/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="SemiEditableField.ts" />

interface CollectionFieldParams {
    container        : JQuery;
    deleteButtonSel  : string;
    onStatusChange?  : Function;
    disableOrdering? : Boolean;
}

class CollectionField {

    private _onStatusChange : Function;
    private _modified : Boolean;
    private _deleteButtonSel : string;

    $container  : JQuery;

    constructor(data : CollectionFieldParams) {
        this._onStatusChange = data.onStatusChange ? data.onStatusChange : function(){};
        this.$container = data.container;
        if (!data.disableOrdering) {
            this.$container.sortable({
                update: $.proxy(function () {
                    if (!this._modified) {
                        this._modified = true;
                        this._onStatusChange(this, this._modified);
                    }
                }, this)
            });
        }
        this.$container.find(data.deleteButtonSel).on('click', $.proxy(this.handleDeleteButton, this));
        this._deleteButtonSel = data.deleteButtonSel;
    }
    addItem(item : JQuery) {
        this.$container.append(item);
        item.find(this._deleteButtonSel).on('click', $.proxy(this.handleDeleteButton, this));
    }
    handleDeleteButton(event : JQueryEventObject) {
        var element = $(event.target).parents('li');
        element.toggleClass('deleted');
        this._modified = true;
        this._onStatusChange(this, this._modified);
    }
    getPrimaryObject() : JQuery {
        return this.$container;
    }
    isModified() : Boolean {
        return this._modified;
    }
    getOrder() : number[] {
        var order : number[] = [];
        this.$container.find('li').each(function() {
            order.push(parseInt($(this).data('id')));
        });
        return order;
    }
    getDeletedItems() : number[] {
        var deleted : number[] = [];
        this.$container.find('li').each(function() {
            if ($(this).hasClass('deleted')) {
                deleted.push(parseInt($(this).data('id')));
            }
        });
        return deleted;
    }
}

interface CollectionOfSemiEditableFieldsParams extends CollectionFieldParams {
    readerSel         : string;
    modifierSel?      : string;
    editButtonSel?    : string;
    indicatorSel?     : string;
    indicatorClasses? : string[];
}

class CollectionOfSemiEditableFields extends CollectionField {
    $semiEditables : Object = {};
    private _data : CollectionOfSemiEditableFieldsParams;
    private _cls : string;
    constructor(cls : string, data : CollectionOfSemiEditableFieldsParams) {
        super(data);
        this._data = data;
        this._cls = cls;
        this.$container.find('li').each($.proxy(function(idx, el) {
            this.registerElement($(el));
        }, this));
    }
    registerElement(li : JQuery) {
        var params : SemiEditableFieldParams = {
            reader         : li.find(this._data.readerSel),
            onStatusChange : $.proxy(function(obj, modified){
                this._modified = true;
                this._onStatusChange(obj, this._modified);
            }, this)
        };
        if (this._data.modifierSel)   params.modifier   = li.find(this._data.modifierSel);
        if (this._data.editButtonSel) params.editButton = li.find(this._data.editButtonSel);
        if (this._data.indicatorSel)  params.indicator  = li.find(this._data.indicatorSel);
        if (typeof this._data.indicatorClasses !== 'undefined') {
            params.indicatorClasses = this._data.indicatorClasses;
        }
        this.$semiEditables[li.data('id')] = new window[this._cls](params);
    }
    getModifiedInputs() {
        var result = {};
        for (var idx in this.$semiEditables) {
            if (this.$semiEditables.hasOwnProperty(idx)) {
                if (this.$semiEditables[idx].isModified()) {
                    result[idx] = this.$semiEditables[idx].getValue();
                }
            }
        }
        return result;
    }
    addItem(item : JQuery) {
        super.addItem(item);
        this.registerElement(item);
    }

}