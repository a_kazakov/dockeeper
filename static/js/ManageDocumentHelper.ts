/// <reference path="../../thirdparty/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/jquery.fileupload/jquery.fileupload.d.ts" />
/// <reference path="SemiEditableField.ts" />
/// <reference path="CollectionField.ts" />

class ManageDocumentsHelper {
    static apiUpdateDocumentUrl : string;
    static apiUploadScanUrl     : string;
    static apiUploadFileUrl     : string;
    static apiAddActionUrl      : string;

    private $submitButton          : JQuery;
    private $loader                : JQuery;
    private $addScansButton        : JQuery;
    private $addFilesButton        : JQuery;
    private $addScansButtonText    : JQuery;
    private $addFilesButtonText    : JQuery;
    private $addActionButton       : JQuery;
    private $addActionLoader       : JQuery;
    private $newActionDateField    : JQuery;
    private $newActionAltDateField : JQuery;
    private $newActionCommentField : JQuery;

    private _date        : SemiEditableField;
    private _tags        : SemiEditableField;
    private _type        : SemiEditableField;
    private _storage     : SemiEditableSelect;
    private _expired     : SemiEditableField;
    private _notes       : SemiEditableField;
    private _scans       : CollectionField;
    private _files       : CollectionOfSemiEditableFields;
    private _actions     : CollectionOfSemiEditableFields;

    private _modifiedFields : number;

    constructor() {
        this.$submitButton = $('#submitButton');
        this.$submitButton.on('click', $.proxy(this.submitChanges, this));
        this.$loader = $('#loader');
        this.$addScansButton = $('#add-scans');
        this.$addFilesButton = $('#add-files');
        this.$addScansButtonText = $('#panel-scans').find('.loader-text');
        this.$addFilesButtonText = $('#panel-files').find('.loader-text');
        this.$newActionDateField = $('#new-action-date');
        this.$newActionAltDateField = $('#new-action-date-alt');
        this.$newActionCommentField = $('#new-action-comment');
        this.$addActionButton = $('#add-action');
        this.$addActionLoader = $('#add-action-loader');

        this.$addScansButton.fileupload({
            dataType : 'json',
            url : ManageDocumentsHelper.apiUploadScanUrl,
            formData : {
                csrfmiddlewaretoken: window['csrf_token']
            },
            start : $.proxy(function() {
                this.$addScansButtonText.addClass('active');
            }, this),
            done : $.proxy(this.handleUploadedScans, this),
            always : $.proxy(function() {
                this.$addScansButtonText.html('&plus;');
                this.$addScansButtonText.removeClass('active');
            }, this),
            progressall : $.proxy(function (e, data) {
                this.$addScansButtonText.text(Math.round(data.loaded / data.total * 100) + "%");
            }, this)
        });
        this.$addFilesButton.fileupload({
            dataType : 'json',
            url : ManageDocumentsHelper.apiUploadFileUrl,
            formData : {
                csrfmiddlewaretoken: window['csrf_token']
            },
            done : $.proxy(this.handleUploadedFiles, this),
            always : $.proxy(function() {
                this.$addFilesButtonText.text('Добавить новый файл');
            }, this),
            progressall : $.proxy(function (e, data) {
                this.$addFilesButtonText.text(Math.round(data.loaded / data.total * 100) + "%");
            }, this)
        });
        this.$addActionButton.on('click', $.proxy(this.addAction, this))
        this.$newActionDateField.datepicker({
            altField: this.$newActionAltDateField,
            altFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            constrainInput: true,
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            numberOfMonths: 3,
            showCurrentAtPos: 1
        }).val("");
        this._date = new SemiEditableDateInput({
            reader         : $("#field-date"),
            editButton     : $("#edit-date"),
            onStatusChange : $.proxy(this.onFieldStatusChange, this)});
        this._tags = new SemiEditableMultiSelect({
            reader         : $("#field-tags"),
            editButton     : $("#edit-tags"),
            onStatusChange : $.proxy(this.onFieldStatusChange, this)});
        this._type = new SemiEditableSelect({
            reader         : $("#field-type"),
            editButton     : $("#edit-type"),
            onStatusChange : $.proxy(this.onFieldStatusChange, this)});
        this._storage = new SemiEditableSelect({
            reader         : $("#field-storage"),
            editButton     : $("#edit-storage"),
            onStatusChange : $.proxy(this.onFieldStatusChange, this)});
        this._expired = new SemiEditableCheckbox({
            reader         : $("#field-expired"),
            editButton     : $("#edit-expired"),
            onStatusChange : $.proxy(this.onFieldStatusChange, this)});
        this._notes = new SemiEditableInput({
            reader         : $("#field-notes"),
            editButton     : $("#edit-notes"),
            onStatusChange : $.proxy(this.onFieldStatusChange, this)});
        this._scans = new CollectionField({
            container       : $("#field-scans"),
            deleteButtonSel : '.delete-button',
            onStatusChange  : $.proxy(this.onFieldStatusChange, this)});
        this._files = new CollectionOfSemiEditableFields('SemiEditableInput', {
            container        : $("#field-files"),
            deleteButtonSel  : '.delete-button',
            editButtonSel    : '.edit-button',
            readerSel        : 'a',
            modifierSel      : 'input',
            indicatorSel     : 'div.wrapper',
            indicatorClasses : ['alert-info', 'alert-success'],
            onStatusChange   : $.proxy(this.onFieldStatusChange, this)});
        this._actions = new CollectionOfSemiEditableFields('SemiEditableActionField', {
            container        : $("#field-actions"),
            deleteButtonSel  : '.delete-button',
            editButtonSel    : '.edit-button',
            readerSel        : '.reader',
            modifierSel      : '.modifier',
            indicatorSel     : 'div.wrapper',
            indicatorClasses : ['alert-info', 'alert-success'],
            onStatusChange   : $.proxy(this.onFieldStatusChange, this),
            disableOrdering  : true});

        this._modifiedFields = 0;
    }
    private addAction() {
        if (this.$newActionCommentField.val() == "") {
            alert('Описание действия должно быть заполнено');
            return;
        }
        if (this.$newActionAltDateField.val() == "") {
            alert('Дата должна быть заполнена');
            return;
        }
        var data = {
            'date' : this.$newActionAltDateField.val(),
            'comment' : this.$newActionCommentField.val(),
            'csrfmiddlewaretoken' : window['csrf_token']
        };
        this.$addActionButton.hide();
        this.$addActionLoader.show();
        $.ajax({
            url         : ManageDocumentsHelper.apiAddActionUrl,
            method      : 'POST',
            data        : data,
            timeout     : 10000,
            dataType    : "JSON",
            traditional : true,
            success     : $.proxy(this.handleAddedAction, this),
            error       : $.proxy(function(jqXHR: JQueryXHR, textStatus: string, errorThrown:string) {
                this.$addActionButton.show();
                this.$addActionLoader.hide();
                this.processAjaxError(jqXHR, textStatus, errorThrown);
            }, this)
        });
    }
    private handleUploadedScans(e, data) {
        var response : Object = data.result;
        if (!response['success']) {
            this.processError(response);
            return;
        }
        this._scans.addItem($(
            '<li data-id="' + response['id'] + '" class="scan-block">' +
                '<div class="delete-button"></div>' +
                '<a class="fancybox" style="background-image: url(\'' + response['thumbnail'] + '\')" href="' + response['image'] + '"></a>' +
            '</li>'));
    }
    private handleUploadedFiles(e, data) {
        var response : Object = data.result;
        if (!response['success']) {
            this.processError(response);
            return;
        }
        var item = $('<li data-id="' + response['id'] + '">' +
                '<div class="alert alert-info wrapper">' +
                    '<div class="icon delete delete-button"><span class="glyphicon glyphicon-remove"></span></div>' +
                    '<div class="icon edit edit-button"><span class="glyphicon glyphicon-pencil"></span></div>' +
                    '<div class="icon restore delete-button"><span class="glyphicon glyphicon-share-alt"></span></div>' +
                    '<div class="extension"></div>' +
                    '<a href="' + response['url'] + '" class="comment"></a>' +
                    '<div class="modifier">' +
                        '<input type="text" class="form-control nodisplay" autocomplete="off">' +
                    '</div>' +
                    '<div class="clearfix"></div>' +
                '</div>' +
            '</li>');
        item.find('input').val(response['comment']);
        item.find('.comment').text(response['comment']);
        item.find('.extension').text(response['extension']);
        this._files.addItem(item);
    }
    private handleAddedAction(response : Object) {
        this.$addActionButton.show();
        this.$addActionLoader.hide();
        if (!response['success']) {
            this.processError(response);
            return;
        }
        var item = $('<li data-id="' + response['id'] + '">' +
            '<div class="alert alert-info wrapper">' +
                '<div class="icon delete delete-button"><span class="glyphicon glyphicon-remove"></span></div>' +
                '<div class="icon edit edit-button"><span class="glyphicon glyphicon-pencil"></span></div>' +
                '<div class="icon restore delete-button"><span class="glyphicon glyphicon-share-alt"></span></div>' +
                '<div class="reader">' +
                    '<div class="date"></div>' +
                    '<span class="comment"></span>' +
                '</div>' +
                '<div class="modifier nodisplay">' +
                    '<table style="width:100%;">' +
                        '<tr><td>' +
                            '<input type="text" class="form-control" autocomplete="off" data-name="comment">' +
                        '</td><td style="width: 100px;">' +
                            '<input type="text" class="form-control" autocomplete="off" data-name="date">' +
                            '<input type="hidden">' +
                        '</td><td style="width: 100px; padding-left:10px;">' +
                            '<button style="width:100%" class="btn btn-success">Готово</button>' +
                        '</td></tr>' +
                    '</table>' +
                '</div>' +
                '<div class="clearfix"></div>' +
            '</div>' +
        '</li>');
        item.find('input[data-name=comment]').val(response['comment']);
        item.find('input[data-name=date]').val(response['date']);
        item.find('.reader .date').text(response['date']);
        item.find('.reader .comment').text(response['comment']);
        this._actions.addItem(item);
    }
    private onFieldStatusChange(obj : any, modified : Boolean) {
        var $panel = obj.getPrimaryObject().parents('div.panel');
        var panel_modified_fields = (typeof $panel.data('modifiedFields') == 'undefined')
            ? 0 : parseInt($panel.data('modifiedFields'));
        if (modified) {
            if (panel_modified_fields == 0) {
                $panel.addClass('panel-success');
            }
            if (this._modifiedFields == 0) {
                this.setChangesStatus('pending');
            }
            ++this._modifiedFields;
            ++panel_modified_fields;
        } else {
            --this._modifiedFields;
            --panel_modified_fields;
            if (panel_modified_fields == 0) {
                $panel.removeClass('panel-success');
            }
            if (this._modifiedFields == 0) {
                this.setChangesStatus('consistent');
            }
        }
        $panel.data('modifiedFields', panel_modified_fields.toString());
    }
    private processError(error : Object) : void {
        alert(error['reason']);
    }
    private processAjaxError(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
        alert("AJAX error: " + errorThrown);
    }
    setChangesStatus(status : string) {
        var $edit_buttons = $(".edit-field");
        switch (status) {
            case 'consistent':
                this.$submitButton.hide();
                this.$loader.hide();
                $edit_buttons.show();
                $(window).off('beforeunload');
                break;
            case 'pending':
                this.$submitButton.show();
                this.$loader.hide();
                $edit_buttons.show();
                $(window).on('beforeunload', function(e) {
                    return 'You have unsaved stuff. Are you sure to leave?';
                });
                break;
            case 'updating':
                this.$submitButton.hide();
                this.$loader.show();
                $edit_buttons.hide();
                $(window).on('beforeunload', function(e) {
                    return 'You have unsaved stuff. Are you sure to leave?';
                });
                break;
        }
    }
    prepareFormData() : Object {
        var res = {};
        var modified = [];
        ['date', 'tags', 'type', 'notes', 'storage', 'expired'].forEach($.proxy(function(field : string){
            var controller = this['_' + field];
            if (controller.isModified()) {
                res[field] = controller.getValue();
                modified.push(field);
            }
        }, this));
        if (this._scans.isModified()) {
            res["scans_order"] = this._scans.getOrder().join(',');
            res["deleted_scans"] = this._scans.getDeletedItems().join(',');
            modified.push("scans");
        }
        if (this._files.isModified()) {
            res["files_order"] = this._files.getOrder().join(',');
            res["deleted_files"] = this._files.getDeletedItems().join(',');
            res["file_renamings"] = JSON.stringify(this._files.getModifiedInputs());
            modified.push("files");
        }
        if (this._actions.isModified()) {
            res["deleted_actions"] = this._actions.getDeletedItems().join(',');
            res["action_updates"] = JSON.stringify(this._actions.getModifiedInputs());
            modified.push("actions");
        }
        res["_modified"] = modified.join(',');
        res['csrfmiddlewaretoken'] = window['csrf_token'];
        return res;
    }
    submitChanges() : void {
        this.setChangesStatus('updating');
        $('body').trigger('stop_editing');
        $.ajax({
            url         : ManageDocumentsHelper.apiUpdateDocumentUrl,
            method      : 'POST',
            data        : this.prepareFormData(),
            timeout     : 10000,
            dataType    : "JSON",
            traditional : true,
            success     : $.proxy(function(response) {
                if (!response['success']) {
                    this.processError(response);
                    this.setChangesStatus('pending');
                    return;
                }
                $(window).off('beforeunload');
                location.reload();
            }, this),
            error       : $.proxy(function(jqXHR: JQueryXHR, textStatus: string, errorThrown:string) {
                this.processAjaxError(jqXHR, textStatus, errorThrown);
                this.setChangesStatus('pending');
            }, this)
        });
    }
}

$(function(){
    window['pageHelper'] = new ManageDocumentsHelper();
});
