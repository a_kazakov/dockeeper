/// <reference path="../../thirdparty/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="SemiEditableField.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CollectionField = (function () {
    function CollectionField(data) {
        this._onStatusChange = data.onStatusChange ? data.onStatusChange : function () {
        };
        this.$container = data.container;
        if (!data.disableOrdering) {
            this.$container.sortable({
                update: $.proxy(function () {
                    if (!this._modified) {
                        this._modified = true;
                        this._onStatusChange(this, this._modified);
                    }
                }, this)
            });
        }
        this.$container.find(data.deleteButtonSel).on('click', $.proxy(this.handleDeleteButton, this));
        this._deleteButtonSel = data.deleteButtonSel;
    }
    CollectionField.prototype.addItem = function (item) {
        this.$container.append(item);
        item.find(this._deleteButtonSel).on('click', $.proxy(this.handleDeleteButton, this));
    };
    CollectionField.prototype.handleDeleteButton = function (event) {
        var element = $(event.target).parents('li');
        element.toggleClass('deleted');
        this._modified = true;
        this._onStatusChange(this, this._modified);
    };
    CollectionField.prototype.getPrimaryObject = function () {
        return this.$container;
    };
    CollectionField.prototype.isModified = function () {
        return this._modified;
    };
    CollectionField.prototype.getOrder = function () {
        var order = [];
        this.$container.find('li').each(function () {
            order.push(parseInt($(this).data('id')));
        });
        return order;
    };
    CollectionField.prototype.getDeletedItems = function () {
        var deleted = [];
        this.$container.find('li').each(function () {
            if ($(this).hasClass('deleted')) {
                deleted.push(parseInt($(this).data('id')));
            }
        });
        return deleted;
    };
    return CollectionField;
})();
var CollectionOfSemiEditableFields = (function (_super) {
    __extends(CollectionOfSemiEditableFields, _super);
    function CollectionOfSemiEditableFields(cls, data) {
        _super.call(this, data);
        this.$semiEditables = {};
        this._data = data;
        this._cls = cls;
        this.$container.find('li').each($.proxy(function (idx, el) {
            this.registerElement($(el));
        }, this));
    }
    CollectionOfSemiEditableFields.prototype.registerElement = function (li) {
        var params = {
            reader: li.find(this._data.readerSel),
            onStatusChange: $.proxy(function (obj, modified) {
                this._modified = true;
                this._onStatusChange(obj, this._modified);
            }, this)
        };
        if (this._data.modifierSel)
            params.modifier = li.find(this._data.modifierSel);
        if (this._data.editButtonSel)
            params.editButton = li.find(this._data.editButtonSel);
        if (this._data.indicatorSel)
            params.indicator = li.find(this._data.indicatorSel);
        if (typeof this._data.indicatorClasses !== 'undefined') {
            params.indicatorClasses = this._data.indicatorClasses;
        }
        this.$semiEditables[li.data('id')] = new window[this._cls](params);
    };
    CollectionOfSemiEditableFields.prototype.getModifiedInputs = function () {
        var result = {};
        for (var idx in this.$semiEditables) {
            if (this.$semiEditables.hasOwnProperty(idx)) {
                if (this.$semiEditables[idx].isModified()) {
                    result[idx] = this.$semiEditables[idx].getValue();
                }
            }
        }
        return result;
    };
    CollectionOfSemiEditableFields.prototype.addItem = function (item) {
        _super.prototype.addItem.call(this, item);
        this.registerElement(item);
    };
    return CollectionOfSemiEditableFields;
})(CollectionField);
//# sourceMappingURL=CollectionField.js.map