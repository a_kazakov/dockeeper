/// <reference path="../../thirdparty/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/jquery.ui.datetimepicker/jquery.ui.datetimepicker.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/select2/select2.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SemiEditableField = (function () {
    function SemiEditableField(data) {
        this._indicatorClasses = typeof data.indicatorClasses !== 'undefined' ? data.indicatorClasses : ['modified'];
        this._onStatusChange = data.onStatusChange ? data.onStatusChange : function () {
        };
        this._modified = false;
        this.$reader = data.reader;
        this.$modifier = data.modifier ? data.modifier : data.reader.next();
        this.$indicator = data.indicator ? data.indicator : data.reader;
        this.$editButton = data.editButton ? data.editButton : data.reader.prev();
        this.$editButton.on('click', $.proxy(this.startEditing, this));
    }
    SemiEditableField.prototype.getPrimaryObject = function () {
        return this.$reader;
    };
    SemiEditableField.prototype.isModified = function () {
        return this._modified;
    };
    SemiEditableField.prototype.getValue = function () {
        throw new Error('Attemt to call abstract method.');
    };
    SemiEditableField.prototype.getText = function () {
        throw new Error('Attemt to call abstract method.');
    };
    SemiEditableField.prototype.updateReader = function () {
        this.$reader.text(this.getText() + " ");
    };
    SemiEditableField.prototype.startEditing = function () {
        this.showModifier();
        if (!this._modified) {
            this._modified = true;
            this._onStatusChange(this, this._modified);
            for (var idx in this._indicatorClasses) {
                this.$indicator.toggleClass(this._indicatorClasses[idx]);
            }
        }
    };
    SemiEditableField.prototype.stopEditing = function () {
        this.updateReader();
        this.showReader();
    };
    SemiEditableField.prototype.showModifier = function () {
        throw new Error('Attemt to call abstract method.');
    };
    SemiEditableField.prototype.showReader = function () {
        throw new Error('Attemt to call abstract method.');
    };
    return SemiEditableField;
})();
var SemiEditableInput = (function (_super) {
    __extends(SemiEditableInput, _super);
    function SemiEditableInput(data) {
        _super.call(this, data);
        if (this.$modifier[0].tagName.toLowerCase() != 'textarea') {
            this.$modifier.on('keydown', function (e) {
                if (e.keyCode == 13) {
                    $(this).blur();
                }
            });
        }
        this.$modifier.on('blur', $.proxy(this.stopEditing, this));
    }
    SemiEditableInput.prototype.getValue = function () {
        return this.$modifier.val();
    };
    SemiEditableInput.prototype.getText = function () {
        return this.$modifier.val();
    };
    SemiEditableInput.prototype.showModifier = function () {
        this.$reader.hide();
        this.$modifier.show().focus();
    };
    SemiEditableInput.prototype.showReader = function () {
        this.$reader.show();
        this.$modifier.hide();
    };
    return SemiEditableInput;
})(SemiEditableField);
var SemiEditableActionField = (function (_super) {
    __extends(SemiEditableActionField, _super);
    function SemiEditableActionField(data) {
        _super.call(this, data);
        this.$submitButton = this.$modifier.find('button');
        this.$commentInput = this.$modifier.find('input[data-name=comment]');
        this.$dateInput = this.$modifier.find('input[data-name=date]');
        this.$altDateInput = this.$dateInput.next();
        this.$dateInput.datepicker({
            altField: this.$altDateInput,
            altFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            constrainInput: true,
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            numberOfMonths: 3,
            showCurrentAtPos: 0
        });
        this.$commentText = this.$reader.find('.comment');
        this.$dateText = this.$reader.find('.date');
        this.$submitButton.on('click', $.proxy(this.stopEditing, this));
        $('body').on('stop_editing', $.proxy(function () {
            this.stopEditing();
        }, this));
    }
    SemiEditableActionField.prototype.getValue = function () {
        return {
            date: this.$altDateInput.val(),
            comment: this.$commentInput.val()
        };
    };
    SemiEditableActionField.prototype.getComment = function () {
        return this.$commentInput.val();
    };
    SemiEditableActionField.prototype.getDate = function () {
        return this.$dateInput.val();
    };
    SemiEditableActionField.prototype.showModifier = function () {
        this.$reader.hide();
        this.$modifier.show().focus();
    };
    SemiEditableActionField.prototype.showReader = function () {
        this.$reader.show();
        this.$modifier.hide();
    };
    SemiEditableActionField.prototype.updateReader = function () {
        this.$commentText.text(this.getComment());
        this.$dateText.text(this.getDate());
    };
    return SemiEditableActionField;
})(SemiEditableField);
var SemiEditableCheckbox = (function (_super) {
    __extends(SemiEditableCheckbox, _super);
    function SemiEditableCheckbox(data) {
        _super.call(this, data);
        this.$modifier.on('blur', $.proxy(this.stopEditing, this));
    }
    SemiEditableCheckbox.prototype.getValue = function () {
        return this.$modifier.is(":checked") ? "on" : "";
    };
    SemiEditableCheckbox.prototype.getText = function () {
        return this.$modifier.is(":checked") ? "Да" : "Нет";
    };
    SemiEditableCheckbox.prototype.showModifier = function () {
        this.$reader.hide();
        this.$modifier.show().focus();
    };
    SemiEditableCheckbox.prototype.showReader = function () {
        this.$reader.show();
        this.$modifier.hide();
    };
    return SemiEditableCheckbox;
})(SemiEditableField);
var SemiEditableDateInput = (function (_super) {
    __extends(SemiEditableDateInput, _super);
    function SemiEditableDateInput(data) {
        _super.call(this, data);
        this.$altField = this.$modifier.next();
        this.$modifier.on('blur', $.proxy(function () {
            setTimeout($.proxy(this.stopEditing, this), 1000);
        }, this));
        this.$modifier.datepicker({
            altField: this.$altField,
            altFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            constrainInput: true,
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            numberOfMonths: 3,
            showCurrentAtPos: 2,
            onSelect: $.proxy(this.stopEditing, this)
        }).val(this.$reader.text());
    }
    SemiEditableDateInput.prototype.getValue = function () {
        return this.$altField.val();
    };
    SemiEditableDateInput.prototype.getText = function () {
        return this.$modifier.val();
    };
    SemiEditableDateInput.prototype.showModifier = function () {
        this.$reader.hide();
        this.$modifier.show().focus();
    };
    SemiEditableDateInput.prototype.showReader = function () {
        this.$reader.show();
        this.$modifier.hide();
    };
    return SemiEditableDateInput;
})(SemiEditableField);
var SemiEditableSelect = (function (_super) {
    __extends(SemiEditableSelect, _super);
    function SemiEditableSelect(data) {
        _super.call(this, data);
        this.$modifier.select2();
        this.$container = this.$modifier.select2('container');
        this.$container.hide();
        this.$modifier.on('select2-blur', $.proxy(this.stopEditing, this));
    }
    SemiEditableSelect.prototype.getValue = function () {
        return this.$modifier.select2('val');
    };
    SemiEditableSelect.prototype.getText = function () {
        return this.$modifier.select2('data').text;
    };
    SemiEditableSelect.prototype.showModifier = function () {
        this.$reader.hide();
        this.$container.show().select2('open');
    };
    SemiEditableSelect.prototype.showReader = function () {
        this.$reader.show();
        this.$container.hide();
    };
    return SemiEditableSelect;
})(SemiEditableField);
var SemiEditableMultiSelect = (function (_super) {
    __extends(SemiEditableMultiSelect, _super);
    function SemiEditableMultiSelect(data) {
        _super.call(this, data);
        this.$modifier.select2();
        this.$container = this.$modifier.select2('container');
        this.$container.hide();
        this.$modifier.on('select2-blur', $.proxy(function () {
            setTimeout($.proxy(function () {
                if (!this.$container.find('input').is(":focus")) {
                    this.stopEditing();
                }
            }, this), 300);
        }, this));
    }
    SemiEditableMultiSelect.prototype.getValue = function () {
        return this.$modifier.select2('val');
    };
    SemiEditableMultiSelect.prototype.updateReader = function () {
        this.$reader.html('<ul class="tags">' + this.$modifier.select2('data').map(function (e) {
            return '<li>' + $('<span>').text(e.text).html() + '</li> ';
        }).join('') + '</ul>');
    };
    SemiEditableMultiSelect.prototype.showModifier = function () {
        this.$reader.hide();
        this.$container.show();
        this.$modifier.select2('open');
    };
    SemiEditableMultiSelect.prototype.showReader = function () {
        this.$reader.show();
        this.$container.hide();
    };
    return SemiEditableMultiSelect;
})(SemiEditableField);
//# sourceMappingURL=SemiEditableField.js.map