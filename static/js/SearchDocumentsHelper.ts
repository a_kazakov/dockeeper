/// <reference path="../../thirdparty/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/jquery.ui.datetimepicker/jquery.ui.datetimepicker.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/select2/select2.d.ts" />

enum RequestType {Auto, Manual}

class SearchDocumentsHelper {

    static apiSearchDocumentsUrl : string;

    _currentAjaxRequest : JQueryXHR;

    $searchForm : JQuery;
    $searchResults : JQuery;

    constructor() {
        this._currentAjaxRequest = null;

        this.$searchForm = $("#search-form");
        this.$searchResults = $("#search-results");

        $('.search-field').on('change', $.proxy(function() {
            this.startAutoSearch();
        }, this));
        this.$searchForm.on('submit', $.proxy(function(e : Event) {
            this.startSearch();
            e.preventDefault();
            return false;
        }, this));
        this.$searchResults.on('click', 'tr', function() {
            window.location.href = $(this).data('href');
        });
        $("input[data-type=date]").each(function() {
            var $e:JQuery = $(this);
            var $new:JQuery = $('<input>').attr('type', 'hidden').attr('name', $e.attr('name'));
            $e.attr('name', '');
            $e.val('');
            $e.after($new);
            $e.datepicker({
                altField: $new,
                altFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                constrainInput: true,
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                numberOfMonths: 3,
                showCurrentAtPos: 2,
            });
            var $reset = $('<a>').attr('href', '#').text('сброс').addClass('reset-date-button').on('click', function() {
                $e.val('');
                $new.val('');
                $e.trigger('change');
                return false;
            });
            $e.before($reset);
        });
        this.startSearch(RequestType.Auto);
    }
    abortRequest() {
        this._currentAjaxRequest.abort();
        this._currentAjaxRequest = null;
    }
    private requestIsAllowed(type : RequestType) : Boolean {
        if (this._currentAjaxRequest === null) {
            return true;
        }
        if (type == RequestType.Manual) {
            this.abortRequest();
            return true;
        }
        return false;
    }
    getFormData() : Object {
        var result = {};
        $.each(this.$searchForm.serializeArray(), function(_, kv) {
            if (result.hasOwnProperty(kv['name'])) {
                result[kv['name']] = $.makeArray(result[kv['name']]);
                result[kv['name']].push(kv['value']);
            } else {
                result[kv['name']] = kv['value'];
            }
        });
        return result;
    }
    startSearch(type : RequestType = RequestType.Manual) : void {
        if (!this.requestIsAllowed(type)) {
            return;
        }
        this.startLoading();
        this._currentAjaxRequest = $.ajax({
            url         : SearchDocumentsHelper.apiSearchDocumentsUrl,
            method      : "POST",
            data        : this.getFormData(),
            timeout     : 10000,
            dataType    : "JSON",
            traditional : true,
            success     : $.proxy(this.processServerResponse, this),
            error       : $.proxy(this.processAjaxError, this)
        });
    }
    displaySearchResults(results : Object) : void {
        var $res = $("<tbody>");
        var $template = $("#templates").find(".search-results-row");
        for (var idx in results) {
            if (results.hasOwnProperty(idx)) {
                var $row = $template.clone();
                $row.data('href', results[idx]['href']);
                if (results[idx]['expired']) {
                    $row.addClass('expired');
                }
                $row.find(".pk").text(results[idx]['pk']);
                $row.find(".date")
                    .text(results[idx]['date'])
                    .data('sortable', results[idx]['canonical_date']);
                $row.find(".notes").text(results[idx]['notes']);
                $row.find(".storage").text(results[idx]['storage']);
                var $ul = $("<ul>");
                $ul.addClass("tags");
                $.each(results[idx]['tags'], function(k : string, v : string) {
                    $ul.append($("<li>").text(v));
                });
                $row.find(".tags").append($ul);
                $res.append($row);
            }
        }
        this.$searchResults.find("tbody").detach();
        this.$searchResults.append($res);
        this.$searchResults.trigger("update");
    }
    startAutoSearch() : void {
        this.startSearch(RequestType.Auto);
    }
    processServerResponse(response : Object) : void {
        this.stopLoading();
        if (!response['success']) {
            this.processError(response);
            return;
        }
        this.displaySearchResults(response['results']);
    }
    processError(response) : void {
        alert(response['reason']);
    }
    processAjaxError(jqXHR : JQueryXHR, textStatus : string, errorThrown : string) {
        if (textStatus == "abort") {
            return;
        }
        this.stopLoading();
        alert('AJAX error');
    }
    startLoading() : void {

    }
    stopLoading() : void {
        this._currentAjaxRequest = null;
    }
}

$(function() {
    window['pageHelper'] = new SearchDocumentsHelper();
});