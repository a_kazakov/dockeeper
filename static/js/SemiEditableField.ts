/// <reference path="../../thirdparty/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/jquery.ui.datetimepicker/jquery.ui.datetimepicker.d.ts" />
/// <reference path="../../thirdparty/DefinitelyTyped/select2/select2.d.ts" />

interface SemiEditableFieldParams {
    reader            : JQuery;
    modifier?         : JQuery;
    indicator?        : JQuery;
    editButton?       : JQuery;
    onStatusChange    : Function;
    indicatorClasses? : string[];
}

class SemiEditableField {

    $reader     : JQuery;
    $modifier   : JQuery;
    $indicator  : JQuery;
    $editButton : JQuery;

    _indicatorClasses : string[];
    _onStatusChange   : Function;
    _modified         : Boolean;

    constructor(data : SemiEditableFieldParams) {
        this._indicatorClasses = typeof data.indicatorClasses !== 'undefined' ? data.indicatorClasses : ['modified'];
        this._onStatusChange = data.onStatusChange ? data.onStatusChange : function(){};
        this._modified = false;

        this.$reader     = data.reader;
        this.$modifier   = data.modifier ? data.modifier : data.reader.next();
        this.$indicator  = data.indicator ? data.indicator : data.reader;
        this.$editButton = data.editButton ? data.editButton : data.reader.prev();

        this.$editButton.on('click', $.proxy(this.startEditing, this));
    }
    getPrimaryObject() : JQuery {
        return this.$reader;
    }
    isModified() : Boolean {
        return this._modified;
    }
    getValue() : any {
        throw new Error('Attemt to call abstract method.');
    }
    getText() : any {
        throw new Error('Attemt to call abstract method.');
    }
    updateReader() : void {
        this.$reader.text(this.getText() + " ");
    }
    startEditing() : void {
        this.showModifier();
        if (!this._modified) {
            this._modified = true;
            this._onStatusChange(this, this._modified);
            for (var idx in this._indicatorClasses) {
                this.$indicator.toggleClass(this._indicatorClasses[idx]);
            }
        }
    }
    stopEditing() : void {
        this.updateReader();
        this.showReader();
    }
    showModifier() : any {
        throw new Error('Attemt to call abstract method.');
    }
    showReader() : any {
        throw new Error('Attemt to call abstract method.');
    }
}

class SemiEditableInput extends SemiEditableField {
    constructor(data : SemiEditableFieldParams) {
        super(data);
        if (this.$modifier[0].tagName.toLowerCase() != 'textarea') {
            this.$modifier.on('keydown', function(e) {
                if (e.keyCode == 13) {
                    $(this).blur();
                }
            });
        }
        this.$modifier.on('blur', $.proxy(this.stopEditing, this));
    }
    getValue() : any {
        return this.$modifier.val();
    }
    getText() : string {
        return this.$modifier.val();
    }
    showModifier() : void {
        this.$reader.hide();
        this.$modifier.show().focus();
    }
    showReader() : void {
        this.$reader.show();
        this.$modifier.hide();
    }
}

class SemiEditableActionField extends SemiEditableField {
    _updater : Function;

    $submitButton : JQuery;
    $commentInput : JQuery;
    $dateInput    : JQuery;
    $altDateInput : JQuery;
    $commentText  : JQuery;
    $dateText     : JQuery;

    constructor(data : SemiEditableFieldParams) {
        super(data);
        this.$submitButton = this.$modifier.find('button');
        this.$commentInput = this.$modifier.find('input[data-name=comment]');
        this.$dateInput = this.$modifier.find('input[data-name=date]');
        this.$altDateInput = this.$dateInput.next();
        this.$dateInput.datepicker({
            altField: this.$altDateInput,
            altFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            constrainInput: true,
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            numberOfMonths: 3,
            showCurrentAtPos: 0
        });
        this.$commentText = this.$reader.find('.comment');
        this.$dateText = this.$reader.find('.date');
        this.$submitButton.on('click', $.proxy(this.stopEditing, this));
        $('body').on('stop_editing', $.proxy(function() {
            this.stopEditing();
        }, this));
    }
    getValue() : any {
        return {
            date : this.$altDateInput.val(),
            comment : this.$commentInput.val()
        };
    }
    getComment() : string {
        return this.$commentInput.val();
    }
    getDate() : string {
        return this.$dateInput.val();
    }
    showModifier() : void {
        this.$reader.hide();
        this.$modifier.show().focus();
    }
    showReader() : void {
        this.$reader.show();
        this.$modifier.hide();
    }
    updateReader() : void {
        this.$commentText.text(this.getComment());
        this.$dateText.text(this.getDate());
    }
}

class SemiEditableCheckbox extends SemiEditableField {
    constructor(data : SemiEditableFieldParams) {
        super(data);
        this.$modifier.on('blur', $.proxy(this.stopEditing, this));
    }
    getValue() : any {
        return this.$modifier.is(":checked") ? "on" : "";
    }
    getText() : string {
        return this.$modifier.is(":checked") ? "Да" : "Нет";
    }
    showModifier() : void {
        this.$reader.hide();
        this.$modifier.show().focus();
    }
    showReader() : void {
        this.$reader.show();
        this.$modifier.hide();
    }
}

class SemiEditableDateInput extends SemiEditableField {
    private $altField : JQuery;
    constructor(data : SemiEditableFieldParams) {
        super(data);
        this.$altField = this.$modifier.next();
        this.$modifier.on('blur', $.proxy(function(){
            setTimeout($.proxy(this.stopEditing, this), 1000);
        }, this));
        this.$modifier.datepicker({
            altField: this.$altField,
            altFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            constrainInput: true,
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            numberOfMonths: 3,
            showCurrentAtPos: 2,
            onSelect: $.proxy(this.stopEditing, this)
        }).val(this.$reader.text());
    }
    getValue() : any {
        return this.$altField.val()
    }
    getText() : string {
        return this.$modifier.val();
    }
    showModifier() : void {
        this.$reader.hide();
        this.$modifier.show().focus();
    }
    showReader() : void {
        this.$reader.show();
        this.$modifier.hide();
    }
}

class SemiEditableSelect extends SemiEditableField {
    private $container : JQuery;
    constructor(data : SemiEditableFieldParams) {
        super(data);
        this.$modifier.select2();
        this.$container = this.$modifier.select2('container');
        this.$container.hide();
        this.$modifier.on('select2-blur', $.proxy(this.stopEditing, this));
    }
    getValue() : any {
        return this.$modifier.select2('val');
    }
    getText() : string {
        return this.$modifier.select2('data').text;
    }
    showModifier() : void {
        this.$reader.hide();
        this.$container.show().select2('open');
    }
    showReader() : void {
        this.$reader.show();
        this.$container.hide();
    }
}

class SemiEditableMultiSelect extends SemiEditableField {
    private $container : JQuery;
    constructor(data : SemiEditableFieldParams) {
        super(data);
        this.$modifier.select2();
        this.$container = this.$modifier.select2('container');
        this.$container.hide();
        this.$modifier.on('select2-blur', $.proxy(function() {
            setTimeout($.proxy(function(){ // megahack
                if (!this.$container.find('input').is(":focus")) {
                    this.stopEditing();
                }
            }, this), 300);
        }, this));
    }
    getValue() : any {
        return this.$modifier.select2('val');
    }
    updateReader() : void {
        this.$reader.html('<ul class="tags">' + this.$modifier.select2('data').map(function(e) {
            return '<li>' + $('<span>').text(e.text).html() + '</li> ';
        }).join('') + '</ul>');
    }
    showModifier() : void {
        this.$reader.hide();
        this.$container.show();
        this.$modifier.select2('open');
    }
    showReader() : void {
        this.$reader.show();
        this.$container.hide();
    }
}
