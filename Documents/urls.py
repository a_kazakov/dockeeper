from django.conf.urls import patterns, url

from Documents import views

urlpatterns = patterns('',
    url(r'^$', views.search_documents, name='search_documents'),
    url(r'^api/search$', views.api_search_documents, name='api_search_documents'),
    url(r'^add$', views.add_document, name='add_document'),
    url(r'^(?P<document_id>\d+)/scans/pdf', views.get_documents_scans_as_pdf, name='get_documents_scans_as_pdf'),
    url(r'^(?P<document_id>\d+)$', views.manage_document, name='manage_document'),
    url(r'^actions$', views.view_upcoming_actions, name='upcoming_actions'),
    url(r'^api/(?P<document_id>\d+)/update$', views.api_update_document, name='api_update_document'),
    url(r'^api/(?P<document_id>\d+)/scans/add$', views.api_add_document_scan, name='api_add_document_scan'),
    url(r'^api/(?P<document_id>\d+)/files/add$', views.api_add_document_file, name='api_add_document_file'),
    url(r'^api/(?P<document_id>\d+)/actions/add$', views.api_add_document_action, name='api_add_document_action'),
)
