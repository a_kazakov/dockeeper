from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def shorten(s):
    arr = str(s).strip().split('\n')[0].split(' ')
    res = ""
    for x in arr:
        t = res + " " + x
        if len(t) > 100:
            return res + ' ...'
        res = t
    return res