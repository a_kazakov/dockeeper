from django import forms
from django.db import models
from django.utils import formats
from django.utils.timezone import now

from DocKeeper import settings
from Documents.templatetags.string_tools import *

import os


class DocumentStorage(models.Model):
    class Meta:
        verbose_name        = 'хранилище документа'
        verbose_name_plural = 'хранилища документов'
        ordering            = ('name', )

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class DocumentTag(models.Model):
    class Meta:
        verbose_name        = 'тег'
        verbose_name_plural = 'теги'
        ordering            = ('name', )

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Document(models.Model):
    class Meta:
        verbose_name        = 'документ'
        verbose_name_plural = 'документы'
        ordering            = ('-date', '-pk')

    date        = models.DateField('Дата документа')
    tags        = models.ManyToManyField(DocumentTag, blank=True, verbose_name='Тэги')
    notes       = models.TextField('Примечания', blank=True)
    storage     = models.ForeignKey(DocumentStorage, verbose_name='Местоположение')
    created     = models.DateTimeField('Дата добавления в систему', default=now)
    expired     = models.BooleanField('Истёк', default=False)

    def __str__(self):
        return self.notes.split('\n')[0]

    def scans(self):
        return self.all_scans.filter(deleted=False)

    def files(self):
        return self.all_files.filter(deleted=False)

    def dict(self):
        res = { k : str(shorten(getattr(self, k))) for k in ['pk', 'notes', 'storage']}
        res['expired'] = self.expired
        res['date'] = formats.date_format(self.date, "DATE_FORMAT")
        res['canonical_date'] = formats.date_format(self.date, "CANONICAL_DATE_FORMAT")
        res['tags'] = [str(x) for x in self.tags.all()]
        return res


class DocumentScan(models.Model):
    class Meta:
        verbose_name        = 'скан документа'
        verbose_name_plural = 'сканы документов'
        ordering            = ('sp', 'pk', )

    document = models.ForeignKey(Document, verbose_name='Документ', related_name='all_scans')
    sp       = models.IntegerField(default=10000, verbose_name='Приоритет при сортировке')
    deleted  = models.BooleanField(default=False, verbose_name='Удалён')

    def __str__(self):
        return "{}: #{}".format(self.document, self.pk)

    def get_image_path(self):
        return os.path.join(settings.MEDIA_ROOT, 'documents', 'images', '{:06}.jpg'.format(self.pk))

    def get_thumbnail_path(self):
        return os.path.join(settings.MEDIA_ROOT, 'documents', 'thumbnails', '{:06}.jpg'.format(self.pk))

    def get_image_url(self):
        return settings.MEDIA_URL + 'documents/images/{:06}.jpg'.format(self.pk)

    def get_thumbnail_url(self):
        return settings.MEDIA_URL + 'documents/thumbnails/{:06}.jpg'.format(self.pk)


class DocumentFile(models.Model):
    class Meta:
        verbose_name        = 'электронный документ'
        verbose_name_plural = 'электронные документы'
        ordering            = ('sp', 'pk', )

    document  = models.ForeignKey(Document, related_name='all_files')
    sp        = models.IntegerField(default=10000, verbose_name='Приоритет при сортировке')
    extension = models.CharField(max_length=64, blank=True)
    comment   = models.CharField(max_length=255)
    deleted  = models.BooleanField(default=False, verbose_name='Удалён')

    def get_file_path(self):
        return os.path.join(settings.MEDIA_ROOT, 'documents', 'files', '{:06}.{}'.format(self.pk, self.extension.lower()))

    def get_file_url(self):
        return settings.MEDIA_URL + 'documents/files/{:06}.{}'.format(self.pk, self.extension.lower())


class DocumentForm(forms.ModelForm):
    class Meta:
        model      = Document
        fields     = ['date', 'tags', 'storage', 'expired', 'notes']


class DocumentSearchForm(forms.Form):
    date_from       = forms.DateField(required=False, label='Начальная дата', widget=forms.TextInput(attrs={'data-type':'date'}))
    date_to         = forms.DateField(required=False, label='Конечная дата', widget=forms.TextInput(attrs={'data-type':'date'}))
    notes           = forms.CharField(max_length=1024, required=False, label='Комментарий (или его часть)')
    tags            = forms.ModelMultipleChoiceField(DocumentTag.objects.all(), required=False, label='Теги')
    storages        = forms.ModelMultipleChoiceField(DocumentStorage.objects.all(), required=False, label='Возможные местоположения')
    show_expired    = forms.BooleanField(required=False, label='Показывать истёкшие')


class DocumentAction(models.Model):
    class Meta:
        verbose_name        = 'запланированное действие'
        verbose_name_plural = 'запланированные действия'
        ordering            = ('date', 'pk')

    document    = models.ForeignKey(Document, verbose_name="Документ", related_name="actions")
    date        = models.DateField(verbose_name="Дата")
    comment     = models.CharField(verbose_name="Комментарий", max_length=1024)


class DocumentActionForm(forms.ModelForm):
    class Meta:
        model   = DocumentAction
        fields  = ['document', 'date', 'comment']
