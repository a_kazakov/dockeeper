from django.contrib import admin

from Documents.models import *

admin.site.register(DocumentStorage)
admin.site.register(DocumentTag)
admin.site.register(Document)
admin.site.register(DocumentScan)
admin.site.register(DocumentFile)
admin.site.register(DocumentAction)
