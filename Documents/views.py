from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponse, HttpResponseRedirect, StreamingHttpResponse
from django.shortcuts import render, get_object_or_404
from PIL import Image, ExifTags

from Documents.models import *
from DocKeeper import settings

from datetime import timedelta
from io import BytesIO

import json
import random
import string
import subprocess as sp
import warnings


def render_html_page(request, template_file, params):
    params['num_upcoming_actions'] = len(DocumentAction.objects.filter(date__lt=now() + timedelta(weeks=1)))
    return render(request, template_file, params)


def handle_uploaded_document_scan(f, new_scan):
    warnings.simplefilter('error', Image.DecompressionBombWarning)
    im = Image.open(BytesIO(f.read()))
    # http://stackoverflow.com/questions/4228530/pil-thumbnail-is-rotating-my-image
    orientation = None
    for orientation in ExifTags.TAGS.keys():
        if ExifTags.TAGS[orientation] == 'Orientation':
            break
    if not orientation is None and hasattr(im, '_getexif') and not im._getexif() is None:
        exif = dict(im._getexif().items())
        if   exif[orientation] == 3: im = im.rotate(180, expand=True)
        elif exif[orientation] == 6: im = im.rotate(270, expand=True)
        elif exif[orientation] == 8: im = im.rotate(90,  expand=True)
    im.save(os.path.join(new_scan.get_image_path()))
    im.thumbnail((190, 190,), Image.ANTIALIAS)
    im.save(os.path.join(new_scan.get_thumbnail_path()))


def api_add_document_scan(request, document_id):
    if request.method != 'POST' or 'file' not in request.FILES:
        return HttpResponse(json.dumps({'success': False, 'reason': 'Nothing submitted'}))
    try:
        with transaction.atomic():
            document = get_object_or_404(Document, pk=document_id)
            scan = DocumentScan.objects.create(document=document)
            handle_uploaded_document_scan(request.FILES['file'], scan)
    except Exception as ex:
        return HttpResponse(json.dumps({'success': False, 'reason': 'Error: ' + str(ex)}))
    return HttpResponse(json.dumps({
        'success'  : True,
        'id'       : scan.pk,
        'thumbnail': scan.get_thumbnail_url(),
        'image'    : scan.get_image_url(),
    }))


def api_add_document_file(request, document_id):
    if request.method != 'POST' or 'file' not in request.FILES:
        return HttpResponse(json.dumps({'success': False, 'reason': 'Nothing submitted'}))
    try:
        with transaction.atomic():
            document = get_object_or_404(Document, pk=document_id)
            name_split = request.FILES['file'].name.split('.')
            file = DocumentFile.objects.create(
                document  = document,
                comment   = name_split[0] if len(name_split) == 1 else '.'.join(name_split[:-1]),
                extension = '' if len(name_split) == 1 else name_split[-1])
            file.save()
            with open(file.get_file_path(), 'wb+') as destination:
                for chunk in request.FILES['file'].chunks():
                    destination.write(chunk)
    except:
        return HttpResponse(json.dumps({'success': False, 'reason': 'Unspecified error'}))
    return HttpResponse(json.dumps({
        'success'  : True,
        'id'       : file.pk,
        'comment'  : file.comment,
        'extension': file.extension,
        'url'      : file.get_file_url(),
    }))


def api_add_document_action(request, document_id):
    if request.method != 'POST' or 'date' not in request.POST and 'comment' not in request.POST:
        return HttpResponse(json.dumps({'success': False, 'reason': 'Nothing submitted'}))
    try:
        form = DocumentActionForm(request.POST.copy())
        form.data["document"] = document_id
        if not form.is_valid():
            return HttpResponse(json.dumps({'success': False, 'reason': 'Form validation error'}))
        action = form.save(commit=True)
    except AssertionError:
        return HttpResponse(json.dumps({'success': False, 'reason': 'Unspecified error'}))
    return HttpResponse(json.dumps({
        'success'  : True,
        'id'       : action.pk,
        'comment'  : action.comment,
        'date'     : formats.date_format(action.date, "DATE_FORMAT"),
    }))


def add_document(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST)
        if form.is_valid():
            document = form.save(commit=True)
            return HttpResponseRedirect(reverse('manage_document', args=(document.pk,)))
    else:
        form = DocumentForm()
    return render_html_page(request, "Documents/add_document.html", {'form': form})


def get_documents_scans_as_pdf(request, document_id):
    document = get_object_or_404(Document, pk=document_id)
    scans = document.scans()
    scan_files = [x.get_image_path() for x in scans]
    dir_name = ''.join(random.choice(string.ascii_lowercase) for _ in range(10))
    os.mkdir(os.path.join(settings.TMP_DIR, dir_name))
    output = os.path.join(settings.TMP_DIR, dir_name, 'document_{}.pdf'.format(document.pk))
    cmd = ['convert'] + scan_files + [
        '-resize', '2362x3271', '-density', '300', '-set', 'units', 'PixelsPerInch', '-gravity', 'Center',
        '-background', 'white', '-extent', '2480x3507'] + [output]
    sp.call(cmd)
    with open(output, 'rb') as f:
        content = BytesIO(f.read())
    os.unlink(output)
    os.rmdir(os.path.join(settings.TMP_DIR, dir_name))
    return StreamingHttpResponse(content, content_type="application/pdf")


def search_documents(request):
    if request.method == 'POST':
        docs, form = api_search_documents(request, True)
    else:
        docs, form = Document.objects.filter(expired=False)[:100], DocumentSearchForm()
    data = {
        'form'     : form,
        'documents': docs
    }
    return render_html_page(request, "Documents/search_documents.html", data)


def api_search_documents(request, get_data=False):
    if request.method != 'POST':
        return HttpResponse(json.dumps({'success': False, 'reason': 'Nothing submitted'}))
    form = DocumentSearchForm(request.POST)
    if not form.is_valid():
        if get_data:
            return [], form
        return HttpResponse(json.dumps({
            'success': False,
            'reason' : 'Form validation failed',
            'errors' : [{'field': k, 'error': str(v[0])} for k, v in form.errors.items()],
        }))
    queryset = Document.objects
    if form.cleaned_data['date_from']:
        queryset = queryset.filter(date__gte=form.cleaned_data['date_from'])
    if form.cleaned_data['date_to']:
        queryset = queryset.filter(date__lte=form.cleaned_data['date_to'])
    if form.cleaned_data['notes']:
        queryset = queryset.filter(notes__icontains=form.cleaned_data['notes'])
    if form.cleaned_data['storages']:
        queryset = queryset.filter(storage__in=form.cleaned_data['storages'])
    if not form.cleaned_data['show_expired']:
        queryset = queryset.filter(expired=False)
    if form.cleaned_data['tags']:
        for tag in form.cleaned_data['tags']:
            queryset = queryset.filter(tags=tag)
    results = queryset.distinct().all()[:100]
    if get_data:
        return results, form
    return HttpResponse(json.dumps({
        'success': True,
        'results': [dict(list(x.dict().items()) +
            [('href', reverse('manage_document', args=(x.pk,)))]) for x in list(results)],
    }))


def manage_document(request, document_id):
    document = get_object_or_404(Document, pk=document_id)
    form = DocumentForm(instance=document)
    return render_html_page(request, "Documents/manage_document.html", {'document': document, 'form': form})


def api_update_document(request, document_id):
    if request.method != 'POST':
        return HttpResponse(json.dumps({'success': False, 'reason': 'Nothing submitted'}))
    try:
        document = Document.objects.get(pk=document_id)
        modified = set(request.POST['_modified'].split(','))
        ref_form = DocumentForm(instance=document)
        raw_data = request.POST.copy()
        print(raw_data)
        for field in ref_form.fields:
            if field not in modified:
                value = ref_form[field].value()
                if type(value) == list:
                    raw_data.setlist(field, [str(x) for x in value])
                else:
                    raw_data[field] = str(value)
        form = DocumentForm(raw_data, instance=document)
        if not form.is_valid():
            return HttpResponse(json.dumps({
                'success': False,
                'reason' : 'Form validation failed',
                'errors' : [{'field': k, 'error': str(v[0])} for k, v in form.errors.items()],
            }))
        form.save()
        if 'scans' in modified:
            scans_order = [int(x) for x in request.POST['scans_order'].split(',')]
            deleted_scans = set() if request.POST['deleted_scans'] == '' else \
                {int(x) for x in request.POST['deleted_scans'].split(',')}
            order_mapping = dict(zip(scans_order, range(len(scans_order))))
            for scan in document.scans():
                if scan.pk in order_mapping:
                    scan.sp = order_mapping[scan.pk]
                if scan.pk in deleted_scans:
                    scan.deleted = True
                scan.save()
        if 'files' in modified:
            files_order = [int(x) for x in request.POST['files_order'].split(',')]
            deleted_files = set() if request.POST['deleted_files'] == '' else \
                {int(x) for x in request.POST['deleted_files'].split(',')}
            file_renamings = json.loads(request.POST['file_renamings'])
            order_mapping = dict(zip(files_order, range(len(files_order))))
            for file in document.files():
                if file.pk in order_mapping:
                    file.sp = order_mapping[file.pk]
                if file.pk in deleted_files:
                    file.deleted = True
                if str(file.pk) in file_renamings:
                    file.comment = file_renamings[str(file.pk)]
                file.save()
        if 'actions' in modified:
            deleted_actions = [] if request.POST['deleted_actions'] == '' else \
                [int(x) for x in request.POST['deleted_actions'].split(',')]
            action_updates = json.loads(request.POST['action_updates'])
            for action_id, patch in action_updates.items():
                try:
                    action = DocumentAction.objects.get(pk=action_id, document=document)
                    if patch['date'] != "":
                        action.date = patch['date']
                    if patch['comment'] != "":
                        action.comment = patch['comment']
                    action.save()
                except:
                    pass
            for action in document.actions.filter(pk__in=deleted_actions):
                action.delete()
    except Document.DoesNotExist:
        return HttpResponse(json.dumps({'success': False, 'reason': 'No such document'}))
    except:
        return HttpResponse(json.dumps({'success': False, 'reason': 'Failed to update models'}))
    return HttpResponse(json.dumps({'success': True}))


def add_document_file(request):
    pass


def remove_document_file(request, document_id):
    document = get_object_or_404(Document, pk=document_id)
    document.file = None
    document.save()
    return HttpResponseRedirect(reverse('manage_document', args=(document.pk,)))


def view_upcoming_actions(request):
    data = {
        'overdue_actions': DocumentAction.objects.filter(date__lt=now()),
        'upcoming_actions': DocumentAction.objects.filter(date__lt=now() + timedelta(weeks=1), date__gte=now()),
    }
    return render_html_page(request, 'Documents/upcoming_actions.html', data)
